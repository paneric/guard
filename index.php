<?php

require 'vendor/autoload.php';

use Paneric\Guard\Guard;

$config = [
    'security' => [
        'guard' => [
            'file_path' => 'temp.txt',
            'algo_password' => PASSWORD_BCRYPT,
            'options_algo_password' => [
                'cost' => 10,
            ],
            'algo_hash' => 'sha512',
            'unique_id_prefix' => '',
            'unique_id_more_entropy' => true,
            'open_ssl_args' => [
                'digest_alg' => 'sha512',
                'private_key_bits' => 4096,
                'private_key_type' => OPENSSL_KEYTYPE_RSA,
            ]
        ],
    ],
];

$guard = new Guard($config['security']['guard']);
