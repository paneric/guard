<?php

declare(strict_types=1);

namespace Paneric\Guard;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;

use Defuse\Crypto\Exception\BadFormatException;
use Defuse\Crypto\Exception\EnvironmentIsBrokenException;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;

use Firebase\JWT\JWT;
use Firebase\JWT\Key as JwtKey;
use RandomLib\Generator as RandomGenerator;

use Paneric\Interfaces\Guard\GuardInterface;
use stdClass;
use TypeError;

class Guard implements GuardInterface
{
    private $randomGenerator;
    private $config;

    private $encryptKey;

    public function __construct(RandomGenerator $randomGenerator, array $config)
    {
        $this->randomGenerator = $randomGenerator;
        $this->config = $config;

        $this->setEncryptKey($config['key_ascii']);
    }

    public function generateRandomString(?int $randomStringLength = null): string
    {
        $randomStringLength = $randomStringLength ?? $this->config['random_string_length'];

        return $this->randomGenerator->generateString($randomStringLength);
    }

    private function setEncryptKey(string $keyAscii): void
    {
        try {
            $this->encryptKey = Key::loadFromAsciiSafeString($keyAscii);
        } catch (BadFormatException $e) {
            echo 'Message: ' .$e->getMessage();
        } catch (EnvironmentIsBrokenException $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }

    public function encrypt(string $data): ?string
    {
        try {
            return  Crypto::encrypt($data, $this->encryptKey);
        } catch (TypeError $e) {
            echo 'Message: ' . $e->getMessage();
        } catch (EnvironmentIsBrokenException $e) {
            echo 'Message: ' .$e->getMessage();
        }

        return null;
    }

    public function decrypt(string $data): ?string
    {
        try {
            return  Crypto::decrypt($data, $this->encryptKey);
        } catch (TypeError $e) {
            echo 'Message: ' . $e->getMessage();
        } catch (EnvironmentIsBrokenException $e) {
            echo 'Message: ' .$e->getMessage();
        } catch (WrongKeyOrModifiedCiphertextException $e) {
            echo 'Message: ' . $e->getMessage();
        }

        return null;
    }

    public function hashPassword(string $password): string
    {
        return password_hash(
            $password,
            $this->config['algo_password'],
            $this->config['options_algo_password']
        );
    }

    public function hash(string $chain): string
    {
        return hash($this->config['algo_hash'], $chain);
    }

    public function verifyHash(string $registeredHash, string $receivedHash): bool
    {
        return hash_equals($registeredHash, $receivedHash);
    }

    public function verifyPassword(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    public function setUniqueId(): string
    {
        return uniqid($this->config['unique_id_prefix'], $this->config['unique_id_more_entropy']);
    }

    public function generatePublicPrivateKeysPair(): array
    {
        // https://www.php.net/manual/en/function.openssl-pkey-new.php

        $resource = openssl_pkey_new($this->config['open_ssl_args']);

        openssl_pkey_export($resource, $privateKey);

        $details = openssl_pkey_get_details($resource);

        return [
            'private_key' => $privateKey,
            'public_key' => $details['key']
        ];
    }

    public function encryptWithPublicKey(string $publicKey, string $data): string
    {
        openssl_public_encrypt($data, $encrypted, $publicKey);

        return $encrypted;
    }

    public function decryptWithPrivateKey(string $privateKey, string $data): string
    {
        openssl_private_decrypt($data, $decrypted, $privateKey);

        return $decrypted;
    }

    public function encodeJwtToken(array $payload): string
    {
        return JWT::encode($payload, $this->config['jwt_secret'], $this->config['algo_jwt_sign']);
    }

    public function decodeJwtToken(string $jwt): array
    {
        return (array) JWT::decode($jwt, new JwtKey($this->config['jwt_secret'], $this->config['algo_jwt_sign']));
    }
}
